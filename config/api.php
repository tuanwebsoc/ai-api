<?php
return [
    'error' => [
        '400' => 'Bad Request',
        '401' => 'Sorry, User Login And Password Is Not Match',
        '403' => 'Forbidden',
        '404' => 'Sorry, The Function Is Not Found',
        '405' => 'Method Not Allowed',
        '408' => 'Sorry, Your Token Is Expired',
        'cannot_generate_token' => 'Sorry, Cannot executed your token!',
    ]
];