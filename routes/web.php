<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/add-data',  ['as' => 'api.add.data', 'uses' => 'CustomersService@postData']);
$router->get('/customers/search',  ['as' => 'api.customers.search.data', 'uses' => 'CustomersService@searchCustomerService']);
// $router->get('/test',  ['as' => 'api.test.data', 'uses' => 'CustomersService@test']);