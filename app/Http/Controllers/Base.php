<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Base extends BaseController
{
    public function getRules($model)
    {
        $rules = [];
        $messages = [];
        // Get all fields and get rules was set on each field
        $fields = $model->getFields();
        
        foreach ($fields as $key => $item) {
            if (!empty($item['validation'])) {
                $rules[$key] = $item['validation'];
            }
            
            if (!empty($item['message'])) {
                $messages = $item['message'];
            }
        }
        
        return [
            'rules' => $rules,
            'message' => $messages
        ];
    }
}