<?php

namespace App\Http\Controllers;

use Exception;
use \Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Validator;

class Controller extends \App\Http\Controllers\Base
{
    public function postData(Request $request)
    {
        // Load cvs data
        $modelCustomer = new Customer();
        
        // Get all post data from request
        $datas = $request->all();
        
        $rules = $this->getRules($modelCustomer);
        
        // validate data
        $validator = Validator::make($datas, $rules['rules'], $rules['message']);
        
        if ($validator->fails()) {
            return [
                'code' => 503,
                'message' => 'validation data',
                'errors' => $validator->errors(),
            ];
        }
        
        $result = $modelCustomer->esInsert($datas);
        
        if ($result['result'] === 'created') {
            return [
                'code' => 200,
                'message' => 'insert completed'
            ];
        } else {
            return [
                'code' => 501,
                'message' => 'insert data fail'
            ];
        }
    }
    
    public function searchCustomerService(Request $request)
    {
        return Customer::searchByParameters([
            'limit' => 50,
            'page' => 0,
        ] + $request->all());
    }
    
    public function initCsv(Request $request)
    {
        $csv = $this->csvToArray(base_path() .'/database/migrations/resolved_conversion.csv');
        
        // Load cvs data
        $modelCustomer = new Customer();
        $data = [];
        
        $reports = [];
        foreach ($csv as $k => $item) {
            if ($k === 0) {
                continue;
            }
            
            $data = [
               'fan_page_id' => uniqid(),
               'fb_user_id' => $item['profile'],
               'conversation_message' => $item['content'],
               'response_message' => $item['response'],
               'response_by' => $item['account'],
               'response_at' => $item['response_at'],
               'tags' => $item['topic_1'],
               'sentiment' => $item['sentiment'],
            ];
            
            try {
                $modelCustomer->esInsert($data);
            } catch (Exception $e) {
                $reports[] = $e->getMessage();
            }
        }
        
        info("====Check error logs==========");
        info(sprintf("There is: %s", count($reports)));
        info($reports);
        info("====End check error==========");
        
        return [
            'message' => 'complete',
            'code' => 200,
            'number_errors' => count($reports),
            'errors' => $reports,
        ];
    }
    
    private function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }
        
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                    else
                        $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        
        return $data;
    }
}
