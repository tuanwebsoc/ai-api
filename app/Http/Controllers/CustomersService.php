<?php

namespace App\Http\Controllers;

use Exception;
use \Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Validator;

class CustomersService extends \App\Http\Controllers\Base
{
    public function postData(Request $request)
    {
        // Load cvs data
        $modelCustomer = new Customer();
        
        // Get all post data from request
        $datas = $request->all();
        
        $rules = $this->getRules($modelCustomer);
        
        // validate data
        $validator = Validator::make($datas, $rules['rules'], $rules['message']);
        
        if ($validator->fails()) {
            return [
                'code' => 503,
                'message' => 'validation data',
                'errors' => $validator->errors(),
            ];
        }
        
        $result = $modelCustomer->esInsert($datas);
        
        if ($result['result'] === 'created') {
            return [
                'code' => 200,
                'message' => 'insert completed'
            ];
        } else {
            return [
                'code' => 501,
                'message' => 'insert data fail'
            ];
        }
    }
    
    public function searchCustomerService(Request $request)
    {
        return Customer::searchByParameters([
            'limit' => 50,
            'page' => 0,
        ] + $request->all(), [
            'body' => [
                'query' => [
                    'query_string' => [
                        'default_operator' => 'OR',
                    ]
                ],
            ]
        ]);
    }
    
    public function test()
    {
         // Insert index 
        $modelCustomer = new \App\Models\Customer();
        
        $result = $modelCustomer->initIndex();
        
        if ((int) $result['acknowledged'] === 1) {
            // Init samples data by csv
            $result = $modelCustomer->initCsv();
            
           return $result;
        } else {
            print_r('can not generate init csv');
        }
    }
}
