<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GlobalServiceProvider extends ServiceProvider
{
    
    public static function returnFalseApi($code)
    {
        switch ($code) {
            case 400: //Bad request
                $message = config('api.error.400');
                break;
            case 401: //Login/Pass
                $message = config('api.error.401');
                break;
            case 403: //Forbidden
                $message = config('api.error.403');
                break;
            case 404: //Not found
                $message = config('api.error.404');
                break;
            case 405: //Method not allow
                $message = config('api.error.405');
                break;
            case 408: //Token expired
                $message = config('api.error.408');
                break;
            default:
                $code = 404;
                $message = config('api.error.404');
                break;
        }
        
        return response()->json(['result' => ['code' => $code, 'data' => []], 'message' => $message]);
    }
    
    public static function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }
        
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                    else
                        $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        
        return $data;
    }
    /**
     * get content cross domain
     * @param <string> $url
     * @param <string> $method
     * @param <array> $params
     * @param <array> $options
     * @param <boolean> $request
     * @return <string>
     * @author TienDQ
     */
    public static function getContent($url, $method = 'get', $params = [], $options = [], $request = null)
    {
        try {
            $content = '';
            $url = trim($url);
            $method = strtolower($method);

            if (function_exists('curl_init')) {
                $options = $options + [
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HEADER => false,
                    CURLOPT_URL => $url,
                    CURLOPT_TIMEOUT => 60,
                ];

                $ch = curl_init();

                if ($request) {
                    $options[CURLOPT_USERAGENT] = $request->server('HTTP_USER_AGENT');
                }

                if ($method == 'post') {
                    $options[CURLOPT_POST] = true;
                    if (!empty($params)) {
                        $options[CURLOPT_POSTFIELDS] = http_build_query($params);
                    }
                }

                if (starts_with($url, 'https')) {
                    $options[CURLOPT_SSL_VERIFYPEER] = false;
                    $options[CURLOPT_SSL_VERIFYHOST] = false;
                }

                curl_setopt_array($ch, $options);
                $content = curl_exec($ch);
                curl_close($ch);
            }

            return $content;
        } catch (\Exception $ex) {
            info($ex->getMessage());

            return '';
        }
    }

    
}
