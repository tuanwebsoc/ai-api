<?php

namespace App\Models;
use App\Providers\GlobalServiceProvider;

class Customer extends ElasticSearch
{
    protected $_index = 'customers_service';
    /**
     * This is table name in elasticsearch
     */
    protected $_table = 'customer';
    
    protected $_analyzer = [
        "analyzer" => [
            "vietnamese_structure_analyzer" => [
                "tokenizer" => "vi_tokenizer",
                "char_filter" =>  ["html_strip"],
                "filter" => [
                  "icu_folding"
                ]
            ],
            "vietnamese_non_structure" => [
                "tokenizer" => "standard",
                "char_filter" =>  ["html_strip"],
                "filter" => [
                  "icu_folding"
                ]
            ]
            
        ]
    ];
    
    protected $fields = [
        'fan_page_id' => [
            'validation' => '',
            'message' => [],
            'properties' => [
                'type' => 'keyword',
            ],
            'field' => 'fan_page_id' // If no field was set, default will be key of current field
        ],
        'fb_user_id' => [
            'validation' => '',
            'message' => [],
            'properties' => [
                'type' => 'keyword',
            ]
        ],
        'conversation_message' => [
            'validation' => 'required',
            'properties' => [
                'type' => 'text',
                "analyzer" => "vietnamese_structure_analyzer"
            ],
        ],
        'response_message' => [
            'validation' => 'required',
            'message' => [
                'response_message.required' => 'Response_message can not empty'
            ],
            'properties' => [
                "type" => "text",
                "analyzer" => "vietnamese_structure_analyzer"
            ]
        ],
        'response_by' => [
            'validation' => 'required',
            'message' => [
                'response_by.required' => 'response_by can not empty'
            ],
            'properties' => [
                'type' => 'text',
                "analyzer" => "vietnamese_non_structure",
            ],
        ],
        'response_at' => [
            'validation' => '',
            'properties' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ]
        ],
        'tags' => [
            'validation' => 'required',
            'message' => [
                'tags.required' => 'tags can not empty'
            ],
            'properties' => [
                'type' => 'text',
                "analyzer" => "vietnamese_non_structure"
            ],
        ],
        'sentiment' => [
            'validation' => 'required',
            'message' => [
                'sentiment.required' => 'sentiment can not empty'
            ],
            'properties' => [
                'type' => 'keyword',
            ]
        ],
    ];
    
    public function initCsv($csvUrl = null)
    {
        if (!$csvUrl) {
            $csvUrl = base_path() .'/database/migrations/resolved_conversion.csv';
        }
        
        $csv = GlobalServiceProvider::csvToArray($csvUrl);
        
        // Load cvs data
        $modelCustomer = new Customer();
        $data = [];
        
        $reports = [];
        
        echo "<pre>";
        print_r($csv[1]);die;
        foreach ($csv as $k => $item) {
            if ($k === 0) {
                continue;
            }
            
            $data = [
               'fan_page_id' => uniqid(),
               'fb_user_id' => $item['profile'],
               'conversation_message' => $item['content'],
               'response_message' => $item['response'],
               'response_by' => $item['account'],
               'response_at' => $item['response_at'],
               'tags' => $item['topic_1'],
               'sentiment' => $item['sentiment'],
            ];
            
            try {
                $modelCustomer->esInsert($data);
            } catch (Exception $e) {
                $reports[] = $e->getMessage();
            }
        }
        
        info("====Check error logs==========");
        info(sprintf("There is: %s", count($reports)));
        info($reports);
        info("====End check error==========");
        
        return [
            'message' => 'complete',
            'code' => 200,
            'number_errors' => count($reports),
            'errors' => $reports,
        ];
    }
}