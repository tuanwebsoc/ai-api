<?php

namespace App\Models\Traits;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\GlobalService;
use App\Providers\GlobalServiceProvider;

trait RestExceptionHandlerTrait
{
    use ApiLogTrait;

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Exception $e)
    {
        if ($this->isModelNotFoundException($e)) {
            return GlobalServiceProvider::returnFalseApi(500);
        } else {
            return GlobalServiceProvider::returnFalseApi(404);
        }
    }

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForExceptionRender($request, $e)
    {
        if ($this->isModelNotFoundException($e)) {
            return GlobalServiceProvider::returnFalseApi(500);
        } else {
            return GlobalServiceProvider::returnFalseApi(404);
        }
    }
    
    /**
     * Determines if the given exception is an Eloquent model not found.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isModelNotFoundException(Exception $e)
    {
        return $e instanceof ModelNotFoundException;
    }

}