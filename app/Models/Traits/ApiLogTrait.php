<?php

namespace App\Models\Traits;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

trait ApiLogTrait
{
    /**
     * Write request log
     *
     * @param Request $request
     * @return bool
     */
    protected function requestLog(Request $request)
    {
        $requestLog = new Logger('Request');
        $requestLog->pushHandler(new StreamHandler(storage_path('logs/api-request.log')), Logger::INFO);
        $requestLog->info('RequestLog', [$request->all()]);

        return true;
    }

    /**
     * Write database log
     *
     * @param $sql
     * @return bool
     */
    protected function databaseLog($sql)
    {
        $requestLog = new Logger('Database');
        $requestLog->pushHandler(new StreamHandler(storage_path('logs/api-database.log')), Logger::INFO);
        $requestLog->info('DatabaseLog', [$sql]);

        return true;
    }

    /**
     * Write response log
     *
     * @param JsonResponse $response
     * @return bool
     */
    protected function responseLog(JsonResponse $response)
    {
        $requestLog = new Logger('Response');
        $requestLog->pushHandler(new StreamHandler(storage_path('logs/api-response.log')), Logger::INFO);
        $requestLog->info('ResponseLog', [$response->content()]);

        return true;
    }
}