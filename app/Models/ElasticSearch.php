<?php

namespace App\Models;

use Exception;

class ElasticSearch extends BaseModels
{
    /**
     * $_query is array query variable
     * @var array
     */
    public static $_query = [];
    
    /**
     * $_sort is array query variable
     * @var array
     */
    public static $_sort = [];
    
    /**
     * Connection method use able for stg or produciton
     * @return mix
     */
    public static function connect()
    {
        try {
            $elasticsearchItems = config('elasticsearch.elasticsearch');
            
            // Create load balancer which can reduce overload for both server
            // Random server
            $numServer = count($elasticsearchItems);
            if ($numServer > 0) {
                $serverNumber = mt_rand(0, $numServer - 1);
            } else {
                $serverNumber = 0;
            }
            
            $server = $elasticsearchItems[$serverNumber];
            
            $hosts = [sprintf('%s:%s', $server['url'], $server['port'])];
            
            return \Elasticsearch\ClientBuilder::create()->setHosts($hosts)->build();
        } catch (Exception $ex) {
            return false;
        }
    }
    
    /**
     * Search method called from search function of elastic search
     * @param array $parameters
     * @return array
     */
    public static function search($parameters = [])
    {
        $client = static::connect();
        $results = [
            'hits' => [
                'total' => 0,
                'hits'  => [],
            ]
        ];
        try {
            if (!$client) {
                return $results;
            } else {
                $results = $client->search($parameters);
            }
            
            return $results;
        } catch (\Exception $e) {
            $error = $e->getMessage();
            info($error);
            $results['errors'] = $error;
            return $results;
        }
    }
    
    /**
     * resetQuery method to clear all value of $_query
     * @return void
     */
    public static function resetQuery()
    {
        // Reset query to null
        static::$_query = [];
    }
    
    /**
     * Get query will implode array from $_query variable to string
     * @return string
     */
    public static function getQuery()
    {
        return implode(' AND ', static::$_query);
    }
    
    /**
     * Getsort function will parse to correct format of elasticsearch
     * @return string
     */
    public static function getSort()
    {
        return static::$_sort;
    }
    
    /**
     * Setsort function will be push sort param to array variable
     * @param string $key
     * @param string $direction
     * @param array $options
     */
    public static function setSort($key, $direction, $options = [])
    {
        $results = [];
        $results[] = [
            $key => array_replace_recursive($options, [
                'order' => $direction
            ])
        ];
        
        static::$_sort = $results;
    }
    
    /**
     * Set query method is written to use with query_string of elasticsearch
     * @param string $key
     * @param string $value
     * @param boolean $useKey
     */
    public static function setQuery($key = '', $value = '', $useKey = true)
    {
        if ($useKey) {
            static::$_query[] = sprintf("%s:(%s)", $key, $value);
        } else {
            static::$_query[] = $value;
        }
    }
    
    /**
     * @param array $fields
     * @param string $type
     * @return void
     */
    public static function setQueryRange($fields = [], $type = 'd')
    {
        $str = '';
        switch ($type) {
            case 'o': // operand syntax
                $str = $fields['field'] . ':(';
                $implodeStr = static::getStringFromTo($fields['value'], $type);
                $str .= $implodeStr;
                $str .= ')';
                break;
            case 'date':
            case 'd': // bracket
                // Do some stuff here
                $str = $fields['field'] . ':[';
                $implodeStr = static::getStringFromTo($fields['value']);
                $str .= $implodeStr;
                $str .= ']';
                break;
            case 'e': // exclude
                $str = $fields['field'] . ':{';
                $implodeStr = static::getStringFromTo($fields['value']);
                $str .= $implodeStr;
                $str .= '}';
                break;
        }
        
        static::$_query[] = $str;
    }
    
    /**
     * getStringFromTo
     * @param array $arrays
     * @param string $type
     * @return string
     */
    public static function getStringFromTo($arrays = [], $type = '')
    {
        switch ($type) {
            case 'o':
                if (!empty($arrays['from']) && empty($arrays['to'])) {
                    $arrays['from'] = '>=' . $arrays['from'];
                } elseif (empty($arrays['from']) && !empty($arrays['to'])) {
                    $arrays['to'] = '<=' . $arrays['to'];
                }
                break;
            default:
                $arrays['from'] = $arrays['from'] === '' ? '*' : $arrays['from'];
                $arrays['to'] = $arrays['to'] === '' ? '*' : $arrays['to'];
                break;
        }
        
        return implode(' TO ', $arrays);
    }
    
    /**
     * searhByParameters
     * @param array $parameters
     * @param array $options
     * @return array
     */
    public static function searchByParameters($parameters = [], $options = [
        'body' => [
            'query' => [
                'query_string' => [
//                     "fields" => ["itemName^5", "itemName.ngram"],
                    'default_operator' => 'AND',
                ]
            ],
            'size' => 30,
            'from' => 0,
        ]
    ])
    {
        // Generate default value
        // $parameters = static::getDefaultParameters($parameters);
        
        $results = [];
        $checkFields = static::fields();
        
        // Filter parameters
        if (count($parameters)) {
            // Begin convert params
            foreach ($parameters as $key => $value) {
                if ($key === 'sort') {
                    // Explode value
                    list($k, $direction) = explode('|', $value);
                    // If sort change to another function
                    static::setSort($k, $direction, [
                        "missing" => "_last"
                    ]);
                    // unset sort
                    unset($parameters[$key]);
                } else {
                    // $key is name of input field
                    $fields = isset($checkFields[$key]) ? $checkFields[$key] : null;
                    $fieldName = static::getFieldName($key);
                    
                    if (!$fields || empty($value)) {
                        continue;
                    }
                    
                    // Continue processing
                    $type = isset($fields['properties']['type']) ? $fields['properties']['type'] : 'undefined';
                    
                    switch ($type) {
                        case 'keyword':
                            // Replace white space by AND
                            $keyword = trim(preg_replace('/\s+/', ' AND ', mb_convert_kana($value, 's')));
                            
                            // Add splash to reserved special character
                            $keyword = static::addSlashList($keyword);
                            
                            static::setQuery($fieldName, $keyword, false);
                            break;
                        default:
                            static::setQuery($fieldName, $value);
                            // Do some stuff
                            break;
                    }
                }
            }
            
            $query = static::getQuery();
            $query = empty($query) ? '*' : $query;
            $sort = static::getSort();
            $sort[] = "_score";
            
            $size = $parameters['limit'];
            $from = (int) $parameters['page'] * $size;
            // $params = static::createParams();
            
            $index = static::getInstance()->_index;
            $type = static::getInstance()->_type;
            $params = array_replace_recursive($options, [
                'index' => $index,
                'type' => $type,
                'body' => [
                    'query' => [
                        'query_string' => [
                            'query' => $query,
                        ]
                    ],
                    'sort' => $sort,
                    'size' => $size,
                    'from' => $from,
                ]
            ]);
            
            $results = static::search($params);
        }
        
        return $results;
    }
    
    /**
     * Filter parameters function will remove empty item
     * @param type $parameters
     * @return type
     */
    public static function filterParameters($parameters = [])
    {
        // filter empty parameters
        // Unset timezone
        if (isset($parameters['timezone'])) {
            unset($parameters['timezone']);
        }
        
        if ((int) $parameters['catid'] === 1) {
            unset($parameters['catid']);
        }
        
        return $parameters;
    }
    
    /**
     * Set parameters default for fields
     * @param array $input
     * @param string $default
     * @return void
     */
    public static function getParameters($input = [], $default = '')
    {
        $parameters = static::$parameters;
        $results = [];
        
        foreach ($parameters as $key => $params) {
            if (isset($params['from']) || isset($params['to'])) {
                $default = [
                    'from' => $params['from'] ?? '',
                    'to' => $params['to'] ?? ''
                ];
            } else {
                $default = '';
            }
            
            // Clean space between of each keyword
            $value = isset($input[$key]) ? $input[$key] : $default;
            if (is_array($value)) {
                foreach ($value as $k => $i) {
                    $value[$k] = trim($i);
                }
            } else {
                $value = trim($value);
            }
            
            $results[$key] = $value;
        }
        
        // Create sort default here
        if (!isset($input['sort'])) {
            $results['sort'] = \Controller_Items_Search::$DEFAULT_SORT;
        } else {
            $results['sort'] = $input['sort'];
        }
        
        if (!isset($input['timezone'])) {
            $results['timezone'] = \Ultilities\Helper::timezoneOffsetString();
        } else {
            $results['timezone'] = $input['timezone'];
        }
        
        return $results;
    }
    
    /**
     * Can set fields just be a checked function
     * @param array $fields
     */
    public static function canSetField($fields = [])
    {
        $can = false;
        foreach ($fields as $k => $value) {
            if (isset($value) && $value !== '' && $value !== null) {
                $can = true;
                break;
            }
        }
        
        return $can;
    }
    
    /**
     * ParseExclude method
     * @param string $value
     * @return string
     */
    public static function parserExclude($value)
    {
        //Convert multibyte japan
        $keyword = trim(preg_replace('/\s+/', ' ', mb_convert_kana($value, 's')));
        $items = explode(' ', $keyword);
        $string = "";
        foreach ($items as $item) {
            if (!empty($item)) {
                $string .= '-' . static::addSlashList($item) . ' ';
            }
        }
        return $string;
    }
    
    /**
     * Rewrite method
     * @param string $keyword
     * @param string $exKeyword
     * @return string
     */
    public static function reWriteAnd($keyword, $exKeyword)
    {
        $keyword   = trim(preg_replace('/\s+/', ' ', mb_convert_kana($keyword, 's')));
        $exKeyword = trim(preg_replace('/\s+/', ' ', mb_convert_kana($exKeyword, 's')));
        $arr   = explode(' ', $keyword);
        $items = explode(' ', $exKeyword);
        
        foreach ($items as $item) {
            if (($key = array_search($item, $arr)) !== false) {
                unset($arr[$key]);
            }
        }
        return implode(' AND ', $arr);
    }
    /**
     * Convert client Date time to Elasticsearch Datetime
     * @param string $dateTime
     * @param string $timeZone
     */
    public static function convertToElasticSearchDateTime($dateTime = '', $timeZone = '+09:00')
    {
        if (empty($dateTime)) {
            return $dateTime;
        }
        
        // Default timezone is japanese time zone
        $template = '%sT%s.000%s'; // Date---T---Time.000---Timezone
        $unixTime = strtotime($dateTime);
        
        // Get date from unixTime
        $date = date('Y-m-d', $unixTime);
        // Get time from unixTime
        $time = date('H:i:s', $unixTime);
        $finalFormat = sprintf($template, $date, $time, $timeZone);
        
        return $finalFormat;
    }
    
    /**
     * AddSlashList method
     * @param string $words
     * @param string $lists
     * @return string
     */
    public static function addSlashList($words = '', $lists = '+-=&|><!(){}[]^"~*?:\/')
    {
        return addcslashes($words, $lists);
    }
    
    public static function fields()
    {
        $instance = new static;
        return $instance->getFields();
    }
    
    public static function getInstance()
    {
        return new static();
    }
    public static function getFieldName($key)
    {
        $fields = static::fields();
        
        if (isset($fields[$key]['field'])) {
            return $fields[$key]['field'];
        } else {
            return $key;
        }
    }
}