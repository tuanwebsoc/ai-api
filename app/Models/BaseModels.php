<?php

namespace App\Models;

use \Elasticsearch\ClientBuilder;
use Exception;

class BaseModels extends \Illuminate\Database\Eloquent\Model
{
    protected $_type = 'doc';
    public function __construct() {
        $hosts = [
            sprintf('%s:%s', env('ES_HOST'), env('ES_PORT')),
        ];
        
        $this->EsClient = ClientBuilder::create()           // Instantiate a new ClientBuilder
                        ->setHosts($hosts)      // Set the hosts
                        ->build();              // Build the client object
    }
    
    /**
     * Insert data only
     * @param array $data array data that we need insert to ES
     * @param string $id custom id that we need insert to ES
     */
    public function esInsert($data = [], $id = null)
    {
        $parameters = [
            'index' => $this->_index,
            'type' => $this->_type,
            'body' => $data
        ];
        
        if (isset($id)) {
            $parameters['id'] = $id;
        }
        
        return $this->EsClient->index($parameters);
    }
    
    /**
     * Insert data only
     * @param array $data array data that we need insert to ES
     * @param string $id custom id that we need insert to ES
     */
    public function esUpdate($data = [], $id = null)
    {
        if (!$id) {
            return false;
        }
        
        $parameters = [
            'index' => $this->_index,
            'type' => $this->_type,
            'id' => $id,
            'body' => [
                'doc' => $data
            ]
        ];
        
        return $this->EsClient->update($parameters);
    }
    
    public function deleteIndex()
    {
        try {
            $parameters = ['index' => $this->_index];
            
            return $this->EsClient->indices()->delete($parameters);
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function initIndex()
    {
        $result = $this->deleteIndex();
        
        $properties = $this->getEsProperties();
        
        $parameters = [
            'index' => $this->_index,
            'body' => [
                'settings' => [
                    "analysis" => $this->_analyzer,
                ],
                'mappings' => [
                    $this->_type => [
                        '_source' => [
                            'enabled' => true
                        ],
                        'properties' => $properties
                    ]
                ],
            ]
        ];
        
        return $this->EsClient->indices()->create($parameters);
    }
    
    public function getEsProperties()
    {
        $returns = [];
        $sourceProperties = $this->fields;
        
        foreach ($sourceProperties as $field => $property) {
            $returns[$field] = $property['properties'];
        }
        
        return $returns;
    }
    
    public function EsSsearch($request)
    {
        $datas = $request->all();
        
        // Begin search
        $params = [
            'index' => $this->_index,
            'type' => $this->_type,
            'body' => [
                'query' => [
                    'match' => $datas
                ]
            ]
        ];
        
        try {
            return $this->EsClient->search($params);
        } catch(\Exception $e) {
            return [
                'code' => 503,
                'message' => 'search was be failure',
                'errors' => $e->getMessage(),
            ];
        }
    }
    
    public function getFields()
    {
        return $this->fields;
    }
}