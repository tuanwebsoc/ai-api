<?php

use Illuminate\Database\Seeder;
use App\Models;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert index 
        $modelCustomer = new Models\Customer();
        
        $result = $modelCustomer->initIndex();
        
        print_r($result);
        
        if ((int) $result['acknowledged'] === 1) {
            // Init samples data by csv
            $result = $modelCustomer->initCsv();
            
            print_r($result);
        } else {
            print_r('can not generate init csv');
        }
    }
}
